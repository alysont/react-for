import Rebase from 're-base'

const base = Rebase.createClass({
  apiKey: "apikey",
  authDomain: "domain.firebaseapp.com",
  databaseURL: "https://dburl.firebaseio.com",
})

export default base
