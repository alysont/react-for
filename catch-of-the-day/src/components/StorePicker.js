import React from 'react'
import { getFunName } from '../helpers'

class StorePicker extends React.Component {
  // constructor() {
  //   super()
  //   this.goToStore = this.goToStore.bind(this)
  // }

  goToStore(event) {
    event.preventDefault()
    console.log('you changed tue url')
    // 1 grab the text from the box
    const storeId = this.storeInput.value
    console.log(`going to ${storeId}`)
    // 2 we are going to transition from / to /store/:store_id
    this.context.router.transitionTo(`/store/${storeId}`)
  }

  render() {
    // any where else
    return (
      <form className="store-selector" onSubmit={(e) => this.goToStore(e)}>
        <h2>Please Enter A store</h2>
        <input type="text" required placeholder="Store Name" defaultValue={getFunName()} ref={(input) => {this.storeInput = input}} />
        <button type="input" >Visit Store ↘</button>
      </form>
    )
  }
}

StorePicker.contextTypes = {
  router: React.PropTypes.object
}

export default StorePicker
